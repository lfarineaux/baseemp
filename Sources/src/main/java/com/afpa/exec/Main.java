package com.afpa.exec;


import java.util.List;
import java.util.Scanner;

import com.afpa.dao.Dao;
import com.afpa.dao.EmpDaoImpl;
import com.afpa.dao.ServDaoImpl;
import com.afpa.modele.Emp;
import com.afpa.modele.Serv;

/**
 * La classe Programme contient la méthode main
 *
 * 
 * @authors ag/lf
 */

public class Main {
	public static void main(String args []) {

		Scanner sc = new Scanner(System.in);
		Dao<Emp> empDao = new EmpDaoImpl();
		Dao<Serv> servDao = new ServDaoImpl();
		String menu = "";
		boolean fin = true;

		/** @param
		 *  menu : numéro saisi dans le menu 
		 *  fin : arrêt du programme
		 */ 

		while (fin) {
			boolean numMenu=false;

			while (!numMenu) {
				System.out.println();
				System.out.println("      **************");
				System.out.println("      *    MENU    *");
				System.out.println("      **************");
				System.out.println();
				System.out.println("0- Arreter le programme");
				System.out.println("1- Ajouter un employé");
				System.out.println("2- Modifier le salaire d'un employé");
				System.out.println("3- Ajouter un service");
				System.out.println("4- Lister les employes d'un service");
				System.out.println("5- Lister les services avec le nombre d'employés par service");
				System.out.println("6- Afficher le plus haut salaire pour chaque service");
				System.out.println("7- Afficher le plus bas salaire pour chaque service");
				System.out.print("-> ");
				menu = sc.next();
				sc.nextLine();
				System.out.println();

				if (menu.length()==1 && Character.isDigit(menu.charAt(0))) {
					numMenu=true;
				}
				else {
					System.out.println("Saisir un chiffre entre 0 et 7.");
				}
			}

			switch (menu) {
			case "0":
				System.out.println("0 - Arrêt du programme");
				fin = false;
				break;

			case "1":
				System.out.println("1- ajouter un employé");
				empDao.save(new Emp());
				break;

			case "2":
				System.out.println("2- modifier le salaire d'un employé");
				empDao.update(new Emp());
				break;

			case "3":
				System.out.println("3- ajouter un service");
				servDao.save(new Serv());
				break;

			case "4":
				System.out.println("4- lister les employes d'un service");
				empDao.getAll();
				break;
			case "5":
				System.out.println("5- lister les services avec le nombre d'employés par service");
				servDao.getAll();
				break;

			case "6":
				System.out.println("6- afficher le plus haut salaire pour chaque service");
				ServDaoImpl.getHighSal();
				break;

			case "7":
				System.out.println("7- afficher le plus bas salaire pour chaque service");
				ServDaoImpl.getLowSal();
				break;

			default:
				System.out.println("Mauvaise saisie");
				break;
			}
		}
		sc.close();
	}
}

