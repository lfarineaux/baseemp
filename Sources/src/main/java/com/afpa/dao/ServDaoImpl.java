package com.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.afpa.connection.MyConnection;
import com.afpa.modele.Serv;

/**
 * La classe ServDaoImpl implémente l'interface Dao et les méthodes à appliquer aux tables
 *
 * 
 * @authors ag/lf
 */

public class ServDaoImpl implements Dao<Serv> {
	private static PreparedStatement preparedStatement;
	static Logger monLogger = LoggerFactory.getLogger(ServDaoImpl.class);

	/** @param
	 *  Inutilisée
	 */ 

	public Serv update(Serv serv) {
		return null;
	}


	/** @param
	 *  lister les services avec le nombre d'employés par service
	 */ 
	public List<Serv> getAll() {
		monLogger.info("Action : Lister les services avec le nombre d'employés par service");
		Serv s=null;
		System.out.println();
		long startTime = System.currentTimeMillis();
		Connection connect = MyConnection.getConnection();
		//		DataSource connect = MyDataSourceFactory.getMySQLDataSource();
		List<Serv> res = null;
		try {
			preparedStatement = connect.prepareStatement("SELECT noserv,service FROM serv;");
			ResultSet result = preparedStatement.executeQuery();
			res = new ArrayList<>();
			int nbPersonne = 0;
			String service="";
			while (result.next()) {

				int noserv = result.getInt("noserv");
				service = result.getString("service");

				String request="SELECT nom FROM emp where noserv='"+noserv+"';";
				preparedStatement = connect.prepareStatement(request);
				ResultSet result2 = preparedStatement.executeQuery();

				while (result2.next()) {
					s=new Serv (noserv,service,null);
					nbPersonne++;
					res.add(s);
				}
				System.out.println("Nombre d'employé : " + nbPersonne+" dans le service "+service);
				nbPersonne = 0;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		long duration=endTime-startTime;
		String mes = "Durée de la requête en ms : "+duration;
		System.out.println();
		monLogger.debug(mes);
		return res;
	}

	/** @param
	 *afficher le plus haut salaire pour chaque service
	 */
	public static ResultSet getHighSal() {
		monLogger.info("Action : Afficher le salaire le plus haut pour chaque service");
		System.out.println();
		long startTime = System.currentTimeMillis();
		Connection connect = MyConnection.getConnection();
		//		DataSource connect = MyDataSourceFactory.getMySQLDataSource();
		List<Serv> res = null;
		ResultSet result=null;
		ResultSet result2=null;
		try {
			preparedStatement = connect.prepareStatement("SELECT noserv FROM serv;");
			result = preparedStatement.executeQuery();
			res = new ArrayList<>();
			while (result.next()) {
				int noserv = result.getInt("noserv");

				String request="select sal FROM emp where sal =(select max(sal) from emp where noserv='"+noserv+"');";
				preparedStatement = connect.prepareStatement(request);
				result2 = preparedStatement.executeQuery();
				while (result2.next()) {
					float sal = result2.getFloat("sal");
					System.out.println("Service : "+noserv + " | Salaire max : " + sal );
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		long duration=endTime-startTime;
		String mes = "Durée de la requête en ms : "+duration;
		System.out.println();
		monLogger.debug(mes);
		return result2;
	}


	/** @param
	 *afficher le plus bas salaire pour chaque service
	 */
	public static ResultSet getLowSal() {
		monLogger.info("Action : Afficher le salaire le plus bas pour chaque service");
		System.out.println();
		long startTime = System.currentTimeMillis();
		Connection connect = MyConnection.getConnection();
		//		DataSource connect = MyDataSourceFactory.getMySQLDataSource();
		ResultSet result=null;
		ResultSet result2=null;
		List<Serv> res = null;
		try {
			preparedStatement = connect.prepareStatement("SELECT noserv FROM serv;");
			result = preparedStatement.executeQuery();
			res = new ArrayList<>();
			while (result.next()) {
				int noserv = result.getInt("noserv");

				String request="select sal FROM emp where sal =(select min(sal) from emp where noserv='"+noserv+"');";
				preparedStatement = connect.prepareStatement(request);
				result2 = preparedStatement.executeQuery();
				while (result2.next()) {
					float sal = result2.getFloat("sal");
					System.out.println("Service : "+noserv + " | Salaire min : " + sal );
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		long duration=endTime-startTime;
		String mes = "Durée de la requête en ms : "+duration;
		System.out.println();
		monLogger.debug(mes);
		return result2;
	}


	/** @param
	 *  Ajouter un service
	 */ 

	public Serv save(Serv ser) {
		monLogger.info("Action : Ajouter un service");
		System.out.println();
		long startTime = System.currentTimeMillis();
		Scanner sc = new Scanner(System.in);
		Connection connect = MyConnection.getConnection();
		//		DataSource connect = MyDataSourceFactory.getMySQLDataSource();
		if (connect != null) {
			try {
				System.out.print(" Saisir le No de service >");
				int noserv = sc.nextInt();
				System.out.print(" Saisir le service >");
				String service = sc.next().toUpperCase();
				sc.nextLine();
				System.out.print(" Saisir la ville >");
				String ville = sc.next().toUpperCase();
				sc.nextLine();

				String request = "INSERT INTO serv VALUES ('"+noserv+"','"+service+"','"+ville+"');";
				preparedStatement = connect.prepareStatement(request);
				int nbr = preparedStatement.executeUpdate();
				if (0 != nbr) {
					System.out.println("Service "+service+" ajouté");
				}
				else {
					System.out.println("Erreur ajout service "+service);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		long endTime = System.currentTimeMillis();
		long duration=endTime-startTime;
		String mes = "Durée de la requête en ms : "+duration;
		System.out.println();
		monLogger.debug(mes);
		return null;
	}
}