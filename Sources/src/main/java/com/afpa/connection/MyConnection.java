package com.afpa.connection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
* La classe MyConnection gère la connexion vers le serveur
*
* 
* @authors ag/lf
*/


public class MyConnection {

	private static String url = "jdbc:postgresql://localhost:5432/jdbc-test";
	private static String utilisateur = "Laurent";
	private static String motDePasse = "sql59";
	private static Connection connexion = null;

	public static void stop() {
		if (connexion != null) {
			try {
				connexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private MyConnection() {
		try {
			Class.forName("org.postgresql.Driver");
			connexion = DriverManager.getConnection( url, utilisateur,
					motDePasse );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection() {
		if (connexion == null) {
			new MyConnection();
		}
		return connexion;
	}

}
