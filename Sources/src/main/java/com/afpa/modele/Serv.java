package com.afpa.modele;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor

/**
 * La classe Serv gère les objets de la base serv
 *
 * 
 * @authors ag/lf
 */



public class Serv{



	private int noserv;	
	private String service;
	private String ville;	


}
