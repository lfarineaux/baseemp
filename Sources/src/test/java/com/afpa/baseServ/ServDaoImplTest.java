package com.afpa.baseServ;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.ResultSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.afpa.dao.Dao;
import com.afpa.dao.ServDaoImpl;
import com.afpa.modele.Serv;

class ServDaoImplTest {
	Dao<Serv> servDao = new ServDaoImpl();
	List <Serv> servList=null;
	ResultSet result=null;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testUpdate() {
	}

	@Test
	void testGetAll() {
		servList=servDao.getAll();
		assertNotEquals(0, servList.size());
	}

	@Test
	void testGetHighSal() {
		result=ServDaoImpl.getHighSal();
		assertNotNull(result);
	}

	@Test
	void testGetLowSal() {
		result=ServDaoImpl.getLowSal();
		assertNotNull(result);
	}

	@Test
	void testSave() {
	
	}

}
