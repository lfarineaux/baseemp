package com.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.afpa.connection.MyConnection;
import com.afpa.connection.MyDataSourceFactory;
import com.afpa.modele.Emp;

/**
 * La classe EmpDaoImpl implémente l'interface Dao et les méthodes à appliquer aux tables
 *
 * 
 * @authors ag/lf
 */

public class EmpDaoImpl implements Dao<Emp> {
	private static PreparedStatement preparedStatement;
	static Logger monLogger = LoggerFactory.getLogger(EmpDaoImpl.class);

	/** @param
	 *  Modifier le salaire d'un employé 
	 */ 
	public Emp update(Emp personne) {
		monLogger.info("Action : Modifier le salaire d'un employé ");
		System.out.println();
		Emp emp=null;
		long startTime = System.currentTimeMillis();
		Scanner sc = new Scanner(System.in);
		Connection connect = MyConnection.getConnection();
//		DataSource connect = MyDataSourceFactory.getMySQLDataSource();
		try {
			System.out.print(" Saisir le nom >");
			String nom = sc.next().toUpperCase();
			sc.nextLine();
			System.out.print(" Saisir le prenom >");
			String prenom = sc.next().toUpperCase();
			sc.nextLine();
			System.out.print(" Saisir le salaire >");
			float sal = sc.nextFloat();

			String request = "UPDATE emp SET sal = '" + sal + "' WHERE nom = '" + nom + "' and prenom = '" + prenom + "';";
			preparedStatement = connect.prepareStatement("UPDATE emp SET sal = '" + sal + "' WHERE nom = '" + nom + "' and prenom = '" + prenom + "';");
			int nbr = preparedStatement.executeUpdate();
			if (0 != nbr) {
				System.out.println("Salaire de l'employé "+nom+" modifié");
				emp = new Emp ();
			}
			else {
				System.out.println("Erreur modification salaire de l'employé "+nom);
			}

		} catch (SQLException  e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		long duration=endTime-startTime;
		String mes = "Durée de la requête en ms : "+duration;
		System.out.println();
		monLogger.debug(mes);
		return emp;
	}


	/** @param
	 *  lister les employés d'un service 
	 */ 
	public List<Emp> getAll() {
		monLogger.info("Action : Lister les employés d'un service");
		System.out.println();
		long startTime = System.currentTimeMillis();
		Scanner sc = new Scanner(System.in);
		Connection connect = MyConnection.getConnection();
//		DataSource connect = MyDataSourceFactory.getMySQLDataSource();
		List<Emp> res = null;
		try {
			System.out.print(" Saisir le No de service >");
			String request = "SELECT * FROM emp where noserv='"+sc.nextInt()+"';";
			preparedStatement = connect.prepareStatement(request);
			ResultSet result = preparedStatement.executeQuery();
			res = new ArrayList<>();
			while (result.next()) {
				int noemp = result.getInt("noemp");
				String nom = result.getString("nom");
				String prenom = result.getString("prenom");
				String emploi = result.getString("emploi");
				int sup = result.getInt("sup");
				Date embauche = result.getDate("embauche");
				float sal = result.getFloat("sal");
				float comm = result.getFloat("comm");		
				int noserv = result.getInt("noserv");

				System.out.println(noemp + " | " + nom + " | " + prenom + " | " + emploi + " | " + sup + " | " + embauche + " | " + sal + " | " + comm + " | " + noserv);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		long duration=endTime-startTime;
		String mes = "Durée de la requête en ms : "+duration;
		System.out.println();
		monLogger.debug(mes);
		return res;
	}


	/** @param
	 *  Ajouter employé 
	 */ 

	public Emp save(Emp personne) {
		monLogger.info("Action : ajouter un employé");
		System.out.println();
		long startTime = System.currentTimeMillis();
		Scanner sc = new Scanner(System.in);
		Connection connect = MyConnection.getConnection();
//		DataSource connect = MyDataSourceFactory.getMySQLDataSource();
				
		if (connect != null) {
			try {

				System.out.print(" Saisir le No d'employe >");
				int noemp = sc.nextInt();
				System.out.print(" Saisir le nom >");
				String nom = sc.next().toUpperCase();
				sc.nextLine();
				System.out.print(" Saisir le prenom >");
				String prenom = sc.next().toUpperCase();
				sc.nextLine();
				System.out.print(" Saisir l'emploi >");
				String emploi = sc.next().toUpperCase();
				sc.nextLine();
				System.out.print(" Saisir le No superieur >");
				int sup = sc.nextInt();
				System.out.print(" Saisir la date d'embauche (yyyy/MM/dd) >");
				String emb = sc.next();
				sc.nextLine();
				Date embauche=new SimpleDateFormat ("yyyy/MM/dd").parse(emb);
				System.out.print(" Saisir le salaire >");
				float sal = sc.nextFloat();
				System.out.print(" Saisir la commission >");
				float comm = sc.nextFloat();
				System.out.print(" Saisir le No de service >");
				int noserv = sc.nextInt();

				String request = "INSERT INTO emp VALUES ('"+noemp+"','"+nom+"','"+prenom+"','"+emploi+"','"+sup+"','"+embauche+"','"+sal+"','"+comm+"','"+noserv+"');";
				preparedStatement = connect.prepareStatement(request);
				int nbr = preparedStatement.executeUpdate();
				if (0 != nbr) {
					System.out.println("Employé "+nom+" ajouté");
				}
				else {
					System.out.println("Erreur ajout employé "+nom);
				}


			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				System.out.println("Mauvaise saisie de la date");
			}
		}
		long endTime = System.currentTimeMillis();
		long duration=endTime-startTime;
		String mes = "Durée de la requête en ms : "+duration;
		System.out.println();
		monLogger.debug(mes);
		return null;
	}
}