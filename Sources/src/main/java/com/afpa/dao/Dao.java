package com.afpa.dao;

import java.util.List;

/**
* L'interface Dao contient les méthodes à appliquer aux tables
*
* 
* @authors ag/lf
*/

public interface Dao<T> {
	T save(T obj);
	T update(T obj);
	List<T> getAll();
}
