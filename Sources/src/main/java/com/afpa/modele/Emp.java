package com.afpa.modele;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class Emp{

	/**
	 * La classe Serv gère les objets de la base serv
	 *
	 * 
	 * @authors ag/lf
	 */


	@NonNull
	private int noemp;
	@NonNull
	private String nom;
	@NonNull
	private String prenom;	
	@NonNull
	private String emploi;	
	//	@NonNull
	private int sup;	
	@NonNull
	private Date embauche;
	//	@NonNull
	private float sal;	
	//	@NonNull
	private float comm;	
	//	@NonNull
	private int noserv;	


}
